// function find(elements, cb) {
//     // Do NOT use .includes, to complete this function.
//     // Look through each value in `elements` and pass each element to `cb`.
//     // If `cb` returns `true` then return that element.
//     // Return `undefined` if no elements pass the truth test.
// }

function find(elements,cb){
    if(Array.isArray(elements)){
    for(index=0;index<elements.length;index++){
       let result= cb(elements[index])
       if(result){
          return result
       }
    } 
}else{
    return "input not array" 
}  
}

function cb(elements){
 if(elements%2===0){
    return true
 }else{
    return false
 }
}


module.exports={find,cb}


