// function flatten(elements) {
//     // Flattens a nested array (the nesting can be to any depth).
//     // Hint: You can solve this using recursion.
//     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }
// function cb(elements,index){
//     console.log(elements+": "+index)
//   }
// each(items,cb)



function flatten(elements){
    if(Array.isArray(elements)){
    let result=[]
  for (const ele of elements) {
    if(Array.isArray(ele)){
       result=result.concat(flatten(ele))
    }else{
        result.push(ele)
    }
  }
  return result
}else{
    return "In suffient data"
}
}


module.exports=flatten